﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniChat.Shared.Client;
using UnityEngine;
using UnityEngine.UI;

public class Chat : MonoBehaviour
{
    public InputField AddressField;
    public InputField PortField;
    public InputField MessageField;
    public Text ChatLogText;
    public Text StatusText;

    private IGameClient _client;


    // Start is called before the first frame update
    void Start()
    {
        _client = new AsyncChatClient();
        _client.OnConnected += (s) => StatusText.text = "Connected";
        _client.OnDisconnected += (s) => StatusText.text = "Disconnected";
        _client.OnError += (s, e) => WriteLine(e.Exception.ToString());
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    private void WriteLine(string line)
    {
        ChatLogText.text += $"\r\n{line}";
    }

    public void Send()
    {
        var message = MessageField.text;
        if (string.IsNullOrEmpty(message) || !_client.Connected) return;
        _client.SendMessageAsync(message);
        WriteLine($"you say {message}");
    }

    public void Connect()
    {
        if (!int.TryParse(PortField.text, out var port))
        {
            return;
        }

        try
        {
            _client.ConnectAsync(AddressField.text, port).Wait();
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
        }
        
    }
}
