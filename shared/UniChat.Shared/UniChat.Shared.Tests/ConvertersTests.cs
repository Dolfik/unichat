using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UniChat.Shared.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Bytes_should_convert_from_short()
        {
            // Arrange
            var values = new Dictionary<short, ArraySegment<byte>>();
            
            // Act
            for (var i = short.MinValue; i < short.MaxValue; i++)
            {
                values[i] = new ArraySegment<byte>(new byte[2]);
                Converters.FromShort(i, values[i].AsSpan());
            }
            
            // Assert
            foreach (var (k, v) in values)
            {
                var b = BitConverter.GetBytes(k);
                if (!BitConverter.IsLittleEndian)
                {
                    b = b.Reverse().ToArray();
                }
                
                Assert.Equal(b, v);
            }
        }
        
        [Fact]
        public void Short_should_convert_to_bytes()
        {
            // Arrange
            var values = new Dictionary<short, ArraySegment<byte>>();
            
            // Act
            for (var i = short.MinValue; i < short.MaxValue; i++)
            {
                values[i] = BitConverter.GetBytes(i);
                if (!BitConverter.IsLittleEndian)
                {
                    values[i] = values[i].Reverse().ToArray();
                }
            }
            
            // Assert
            foreach (var (k, v) in values)
            {
                var s = Converters.ToShort(v);
                Assert.Equal(k, s);
            }
        }

        [Fact]
        public void Int_should_convert_to_bytes()
        {
            // Arrange
            var values = new Dictionary<int, ArraySegment<byte>>();

            // Act
            for (int i = short.MinValue; i < short.MaxValue; i++)
            {
                values[i] = BitConverter.GetBytes(i);
                if (!BitConverter.IsLittleEndian)
                {
                    values[i] = values[i].Reverse().ToArray();
                }
            }

            // Assert
            foreach (var (k, v) in values)
            {
                var s = Converters.ToInt(v);
                Assert.Equal(k, s);
            }
        }
    }
}