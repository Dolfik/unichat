using System.IO.Pipelines;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UniChat.Shared.Transport;

namespace UniChat.Shared
{
    public class SocketPipe : ISocketPipe
    {
        private const int MinimumBufferSize = 512;

        private readonly PipeScheduler _pipeScheduler;
        private readonly Pipe _inputPipe = new Pipe();
        private readonly Pipe _outputPipe = new Pipe();

        private readonly Socket _socket;
        
        public PipeReader Input => _inputPipe.Reader;
        public PipeWriter Output => _outputPipe.Writer;
        public bool Connected => _socket.Connected;

        public SocketPipe(
            PipeScheduler pipeScheduler,
            SocketType socketType = SocketType.Stream,
            ProtocolType protocolType = ProtocolType.Tcp) : this(pipeScheduler)
        {
            _socket = new Socket(socketType, protocolType);
        }

        public SocketPipe(
            PipeScheduler pipeScheduler,
            EndPoint endPoint,
            AddressFamily addressFamily = AddressFamily.InterNetwork,
            SocketType socketType = SocketType.Stream,
            ProtocolType protocolType = ProtocolType.Tcp) : this(pipeScheduler)
        {
            _socket = new Socket(addressFamily, socketType, protocolType);
            _socket.Bind(endPoint);
        }

        private SocketPipe(PipeScheduler pipeScheduler)
        {
            _pipeScheduler = pipeScheduler;
        }

        public async Task ConnectAsync(EndPoint endPoint)
        {
            await _socket.ConnectAsync(endPoint);
        }

        public async Task ConnectAsync(string host, int port)
        {
            await _socket.ConnectAsync(host, port);
        }
        
        public async Task ConnectAsync(IPAddress address, int port)
        {
            await _socket.ConnectAsync(address, port);
        }
        
        public Task DisconnectAsync()
        {
            _socket.Disconnect(false);
            return Task.CompletedTask;
        }
        
        public void BeginReceive()
        {
            using (var receiver = new SocketReceiver(_socket, _pipeScheduler))
            {
                SocketAwaitableEventArgs args;
                do
                {
                    var memory = _inputPipe.Writer.GetMemory(MinimumBufferSize);
                    args = receiver.ReceiveAsync(memory);
                    _inputPipe.Writer.Advance(memory.Length);
                } while (!args.IsCompleted);
            }
        }

        public async Task SendAsync()
        {
            using (var sender = new SocketSender(_socket, _pipeScheduler))
            {
                SocketAwaitableEventArgs args;
                do
                {
                    if(!_outputPipe.Reader.TryRead(out var result))
                    {
                        var read = _outputPipe.Reader.ReadAsync();
                        result = await read;
                    }
                    args = sender.SendAsync(result.Buffer);
                    _outputPipe.Reader.AdvanceTo(result.Buffer.End);
                } while (!args.IsCompleted);
            }
        }
        
        public void Listen(int listenBacklog = 20)
        {
            _socket.Listen(listenBacklog);
        }
        
        public void Dispose()
        {
            _socket.Dispose();
        }

        public override int GetHashCode()
        {
            return _socket.GetHashCode();
        }
    }
}