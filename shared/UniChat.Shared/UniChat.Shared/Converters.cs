using System;
using System.Buffers;
using System.Buffers.Binary;
using System.Runtime.CompilerServices;
using System.Text;
using UniChat.Shared.Extensions;

namespace UniChat.Shared
{
    public static class Converters
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void FromShort(short number, Span<byte> span)
        {
            BinaryPrimitives.WriteInt16LittleEndian(span, number);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void FromInt(int number, Span<byte> span)
        {
            BinaryPrimitives.WriteInt32LittleEndian(span, number);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static short ToShort(ReadOnlySpan<byte> span)
        {
            return BinaryPrimitives.ReadInt16LittleEndian(span);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int ToInt(ReadOnlySpan<byte> span)
        {
            return BinaryPrimitives.ReadInt32LittleEndian(span);
        }

        public static string ToUtf8String(ReadOnlySpan<byte> span)
        {
            return Encoding.UTF8.GetString(span.ToArray());
        }

        public static string ToUtf8String(ReadOnlySequence<byte> buffer)
        {
            return Encoding.UTF8.GetString(buffer.ToArray());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void FromUtf8String(string message, Span<byte> span)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            foreach (var b in buffer)
            {
                span.Fill(b);
            }
        }
    }
}