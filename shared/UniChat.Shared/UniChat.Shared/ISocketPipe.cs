using System;
using System.IO.Pipelines;
using System.Net;
using System.Threading.Tasks;

namespace UniChat.Shared
{
    public interface ISocketPipe : IDuplexPipe, IDisposable
    {
        bool Connected { get; }
        void Listen(int listenBacklog = 20);

        Task ConnectAsync(EndPoint endPoint);
        Task ConnectAsync(IPAddress address, int port);
        Task ConnectAsync(string host, int port);


        void BeginReceive();
        Task SendAsync();
    }
}