﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace UniChat.Shared.Client
{
    public interface IGameClient 
        : IDisposable
    {
        
        event ClientEventHandler OnConnected;
        event ClientEventHandler OnDisconnected;
        event ClientErrorEventHandler OnError;
        event MessageArrivedEventHandler OnMessageArrived;
        
        bool Connected { get; }
        Task ConnectAsync(IPAddress address, int port);
        Task ConnectAsync(string host, int port);
        Task SendMessageAsync(string message);
        Task DisconnectAsync();
    }
}