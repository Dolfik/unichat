using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UniChat.Shared.Client
{
    public class AsyncChatClient : IGameClient
    {
        private readonly Socket _socket;
        
        public event ClientEventHandler OnConnected;
        public event ClientEventHandler OnDisconnected;
        public event ClientErrorEventHandler OnError;
        public event MessageArrivedEventHandler OnMessageArrived;

        public bool Connected => _socket.Connected;

        public AsyncChatClient(
            SocketType socketType = SocketType.Stream,
            ProtocolType protocolType = ProtocolType.Tcp)
        {
            _socket = new Socket(socketType, protocolType);
        }

        public async Task ConnectAsync(IPAddress address, int port)
        {
            await _socket.ConnectAsync(address, port);
            OnClientConnected();
        }

        public async Task ConnectAsync(string host, int port)
        {
            await _socket.ConnectAsync(host, port);
            OnClientConnected();
        }
        
        private void BeginReceive()
        {
            var state = new byte[sizeof(short)];
            _socket.BeginReceive(state, 0, state.Length, SocketFlags.Partial, OnPayloadLengthReceived, state);
        }

        private void OnPayloadLengthReceived(IAsyncResult result)
        {
            var state = (byte[]) result.AsyncState;
            var payloadLength = Converters.ToShort(state);
            state = new byte[payloadLength];
            _socket.BeginReceive(state, 0, state.Length, SocketFlags.None, OnPayloadDataReceived, state);
        }

        private void OnPayloadDataReceived(IAsyncResult result)
        {
            _socket.EndReceive(result);
            BeginReceive();
            var state = new ArraySegment<byte>((byte[])result.AsyncState);
            var clientId = Converters.ToInt(state.AsSpan(0, sizeof(int)));
            var message = Converters.ToUtf8String(state.AsSpan(sizeof(int)));
            
            OnMessageArrived?.Invoke(this, new MessageArrivedEventArgs(clientId, message));
        }

        public async Task SendMessageAsync(string message)
        {
            
            var messageLength = Encoding.UTF8.GetByteCount(message);
            var packetLength = sizeof(short) + messageLength;
            var buffer = new ArraySegment<byte>(new byte[packetLength]);
            
            Converters.FromShort((short)(messageLength), buffer);
            Converters.FromUtf8String(message, buffer.AsSpan(sizeof(short)));

            var cs = new TaskCompletionSource<bool>();
            _socket.BeginSend(buffer.Array, 0, packetLength, SocketFlags.None, OnSent, cs);
            
            await cs.Task;
        }

        private void OnSent(IAsyncResult asyncResult)
        {
            var cs = (TaskCompletionSource<bool>) asyncResult.AsyncState;
            _socket.EndSend(asyncResult);
            cs.SetResult(true);
        }

        private void OnClientConnected()
        {
            BeginReceive();
            OnConnected?.Invoke(this);
        }

        public Task DisconnectAsync()
        {
            _socket.Disconnect(false);
            OnDisconnected?.Invoke(this);
            return Task.CompletedTask;
        }
        
        public void Dispose()
        {
            _socket.Dispose();
        }
    }
}