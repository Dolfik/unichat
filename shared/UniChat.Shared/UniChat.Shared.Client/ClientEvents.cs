using System;

namespace UniChat.Shared.Client
{
    public delegate void ClientEventHandler(object source);
    public delegate void ClientErrorEventHandler(object source, ErrorEventArgs eventArgs);
    public delegate void MessageArrivedEventHandler(object source, MessageArrivedEventArgs eventArgs);
    
    public class MessageArrivedEventArgs : EventArgs
    {
        public readonly int ClientId;
        public readonly string Message;
        
        public MessageArrivedEventArgs(int clientId, string message)
        {
            ClientId = clientId;
            Message = message;
        }
    }

    public class ErrorEventArgs : EventArgs
    {
        public readonly Exception Exception;

        public ErrorEventArgs(Exception exception)
        {
            Exception = exception;
        }
    }
}