using System;
using System.IO.Pipelines;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UniChat.Shared.Extensions;

namespace UniChat.Shared.Client
{
    public class PipelinesChatClient : IGameClient
    {
        private readonly SocketPipe _socketPipe;
        
        public event ClientEventHandler OnConnected;
        public event ClientEventHandler OnDisconnected;
        public event ClientErrorEventHandler OnError;
        public event MessageArrivedEventHandler OnMessageArrived;
        
        public bool Connected => _socketPipe.Connected;
        
        public PipelinesChatClient()
        {
            _socketPipe = new SocketPipe(PipeScheduler.ThreadPool);
        }

        public async Task ConnectAsync(IPAddress address, int port)
        {
            if (Connected)
            {
                throw new InvalidOperationException("Client already connected");
            }

            await _socketPipe.ConnectAsync(address, port);
            OnConnected?.Invoke(this);
            _ = Task.Run(WaitForMessages);
        }
        
        public async Task ConnectAsync(string host, int port)
        {
            if (Connected)
            {
                throw new InvalidOperationException("Client already connected");
            }

            await _socketPipe.ConnectAsync(host, port);
            OnConnected?.Invoke(this);
        }

        public async Task DisconnectAsync()
        {
            if (!Connected)
            {
                throw new InvalidOperationException("Client is not connected");
            }

            await _socketPipe.DisconnectAsync();
            OnDisconnected?.Invoke(this);
        }

        public async Task SendMessageAsync(string message)
        {
            var messageLength = Encoding.UTF8.GetByteCount(message);
            var packetLength = sizeof(short) + messageLength;
            var memory = _socketPipe.Output.GetMemory(packetLength);
            
            Converters.FromShort((short)(messageLength), memory.Span);
            Converters.FromUtf8String(message, memory.Slice(sizeof(short)).Span);
            
            _socketPipe.Output.Advance(packetLength);
            
            await _socketPipe.Output.WriteAsync(memory);
            await _socketPipe.Output.FlushAsync();
            await _socketPipe.SendAsync();
        }

        private async Task WaitForMessages()
        {
            _socketPipe.BeginReceive();
            try
            {
                while (true)
                {
                    var result = await _socketPipe.Input.ReadAsync();
                    var payloadLengthBuffer = result.Buffer.Slice(0, sizeof(short));
                    var payloadLength = Converters.ToShort(payloadLengthBuffer.ToSpan());

                    var clientIdBuffer = result.Buffer.Slice(sizeof(short), sizeof(int));
                    var clientId = Converters.ToInt(clientIdBuffer.ToSpan());

                    var payloadMessageBuffer =
                        result.Buffer.Slice(sizeof(short) + sizeof(int), payloadLength - sizeof(int));
                    
                    OnMessageArrived?.Invoke(this, new MessageArrivedEventArgs(
                        clientId,
                        Converters.ToUtf8String(payloadMessageBuffer)));
                }
            }
            catch (Exception ex)
            {
                OnError?.Invoke(this, new ErrorEventArgs(ex));
            }
        }

        public void Dispose()
        {
            _socketPipe?.Dispose();
        }
    }
}