﻿using System;
using System.Net;
using System.Threading.Tasks;
using UniChat.Shared.Client;

namespace UniChat.Shared.ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            const string enterMessage = "Enter message to send (or :exit to close console): ";

            IGameClient client;
            
            // client = new PipelinesChatClient();
            client = new AsyncChatClient();
            
            client.OnConnected += source => Console.WriteLine("Connected!");
            client.OnDisconnected += source => Console.WriteLine("Disconnected!");
            client.OnError += (source, e) => Console.WriteLine(e.Exception);
            client.OnMessageArrived += (source, e) =>
            {
                Console.WriteLine($"{e.ClientId} say {e.Message}");
                Console.WriteLine(enterMessage);
            };
            
            await client.ConnectAsync(IPAddress.Loopback, 1234);

            var line = string.Empty;
            while (client.Connected && line != ":exit")
            {
                Console.WriteLine(enterMessage);
                line = Console.ReadLine();
                await client.SendMessageAsync(line);
            }
            
            if (client.Connected)
            {
                await client.DisconnectAsync();
            }
        }
    }
}