﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UniChat.Server.Core;

namespace UniChat.Server.AsyncServer
{
    public class AsyncGameServer : SocketServer<AsyncClientConnection>
    {
        public List<AsyncClientConnection> ConnectedClients { get; } = new List<AsyncClientConnection>();

        public override Task OnClientConnectedAsync(AsyncClientConnection client)
        {
            Console.WriteLine($"Client {client.GetHashCode()} is connected");
            ConnectedClients.Add(client);
            return Task.CompletedTask;
        }

        public override void OnStarted(EndPoint endPoint) { }

        public override Task OnClientDisconnectedAsync(AsyncClientConnection client)
        {
            Console.WriteLine($"Client {client.GetHashCode()} was disconnected!");
            ConnectedClients.Remove(client);
            return Task.CompletedTask;
        }

        public override void OnClientFaulted(AsyncClientConnection client, Exception exception)
        {
            Console.WriteLine($"Client {client.GetHashCode()} was Faulted!\r\n{exception}");
            ConnectedClients.Remove(client);
        }

        public override void OnServerFaulted(Exception exception)
        {
            base.OnServerFaulted(exception);
            Console.WriteLine(exception);
        }
        
        public override AsyncClientConnection CreateConnection(Socket socket)
        {
            var connection = new AsyncClientConnection(this, socket);
            ConnectedClients.Add(connection);
            return connection;
        }
    }
}