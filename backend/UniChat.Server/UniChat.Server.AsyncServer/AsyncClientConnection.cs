using System;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using UniChat.Server.Core;
using UniChat.Shared;

namespace UniChat.Server.AsyncServer
{
    public class AsyncClientConnection : SocketClientConnection
    {
        private readonly ArraySegment<byte> _id;

        private readonly AsyncGameServer _asyncGameServer;
        
        public AsyncClientConnection(
            AsyncGameServer asyncGameServer,
            Socket socket) : base(socket)
        {
            _id = new ArraySegment<byte>(BitConverter.GetBytes(socket.GetHashCode()));
            _asyncGameServer = asyncGameServer;
            BeginReceive();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void BeginReceive()
        {
            var state = new byte[sizeof(short)];
            Socket.BeginReceive(
                state, 0,
                sizeof(short), SocketFlags.Partial, 
                ReceivePayloadLengthCallback, state);
        }
        
        private void ReceivePayloadLengthCallback(IAsyncResult asyncResult)
        {
            var state = asyncResult.AsyncState as byte[];
            var payloadLength = Converters.ToShort(state);

            state = new byte[payloadLength];
            Socket.BeginReceive(
                state, 0, payloadLength,
                SocketFlags.None, ReceivePayloadCallback, state);
        }

        private void ReceivePayloadCallback(IAsyncResult asyncResult)
        {
            var state = asyncResult.AsyncState as byte[];
            Socket.EndReceive(asyncResult);
            var buffer = new ArraySegment<byte>(new byte[sizeof(short) + sizeof(int) + state.Length]);

            Converters.FromShort(
                (short) (buffer.Count - sizeof(short)),
                buffer.AsSpan(0, sizeof(short)));
            
            _id.AsSpan().CopyTo(buffer.AsSpan(sizeof(short), _id.Count));
            state.CopyTo(buffer.AsSpan(sizeof(short) + _id.Count, state.Length));
            
            foreach (var client in _asyncGameServer.ConnectedClients
                .Where(x => x != this))
            {
                client.Socket.BeginSend(
                    buffer.Array, 0, buffer.Count, SocketFlags.None,
                    client.SendPayloadCallback, null);
            }
            
            BeginReceive();
        }

        private void SendPayloadCallback(IAsyncResult asyncResult)
        {
            Socket.EndSend(asyncResult);
        }
    }
}