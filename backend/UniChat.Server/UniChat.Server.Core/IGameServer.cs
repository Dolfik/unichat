using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace UniChat.Server.Core
{
    public interface IGameServer<TClientConnection> : IDisposable
        where TClientConnection : IClientConnection
    {
        void Listen(
            EndPoint endPoint,
            AddressFamily addressFamily = AddressFamily.InterNetwork,
            SocketType socketType = SocketType.Stream,
            ProtocolType protocolType = ProtocolType.Tcp,
            int listenBacklog = 20);

        void OnStarted(EndPoint endPoint);
        
        Task StopAsync();

        void OnServerFaulted(Exception exception);
        void OnClientFaulted(TClientConnection client, Exception exception);
        Task OnClientConnectedAsync(TClientConnection client);
        Task OnClientDisconnectedAsync(TClientConnection client);
        
        bool IsListening { get; set; }
    }
}