using System;
using System.Threading.Tasks;

namespace UniChat.Server.Core
{
    public interface IClientConnection : IDisposable
    {
        Task DisconnectAsync();
    }
}