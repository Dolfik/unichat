using System.Net.Sockets;
using System.Threading.Tasks;

namespace UniChat.Server.Core
{
    public abstract class SocketClientConnection : IClientConnection
    {
        protected readonly Socket Socket;

        protected SocketClientConnection(Socket socket)
        {
            Socket = socket;
        }

        public override int GetHashCode()
        {
            return Socket.GetHashCode();
        }

        public Task DisconnectAsync()
        {
            // TODO
            Socket.Disconnect(false);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Socket.Dispose();
        }
    }
}