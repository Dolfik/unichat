using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace UniChat.Server.Core
{
    public abstract class SocketServer<TClientConnection> : IGameServer<TClientConnection>
        where TClientConnection : SocketClientConnection
    {
        public bool IsListening { get; set; }
        private Socket _listener;

        public void Listen(EndPoint endPoint, AddressFamily addressFamily = AddressFamily.InterNetwork,
            SocketType socketType = SocketType.Stream, ProtocolType protocolType = ProtocolType.Tcp, int listenBacklog = 20)
        {
            if (_listener != null) throw new InvalidOperationException("Server is already running");
            var listener = new Socket(addressFamily, socketType, protocolType);
            listener.Bind(endPoint);
            listener.Listen(listenBacklog);

            _listener = listener;
            var _ = ListenAsync();
            OnStarted(endPoint);
        }

        public Task StopAsync()
        {
            IsListening = false;
            return Task.CompletedTask;
        }

        public virtual void OnServerFaulted(Exception exception)
        {
            IsListening = false;
        }

        public abstract Task OnClientDisconnectedAsync(TClientConnection client);
        public abstract void OnClientFaulted(TClientConnection client, Exception exception);
        public abstract Task OnClientConnectedAsync(TClientConnection client);
        public abstract void OnStarted(EndPoint endPoint);

        public void Dispose()
        {
            _listener.Dispose();
        }

        private async Task ListenAsync()
        {
            try
            {
                IsListening = true;
                while (IsListening)
                {
                    var clientSocket = await _listener.AcceptAsync()
                        .ConfigureAwait(false);
                    var client = CreateConnection(clientSocket);
                    var _ = OnClientConnectedAsync(client);
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e);
            }
            catch (ObjectDisposedException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception ex) { OnServerFaulted(ex); }
        }

        public abstract TClientConnection CreateConnection(Socket socket);
    }
}