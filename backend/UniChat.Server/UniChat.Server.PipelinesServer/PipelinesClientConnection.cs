using System;
using System.IO.Pipelines;
using System.Net.Sockets;
using System.Threading.Tasks;
using UniChat.Server.Core;

namespace UniChat.Server.PipelinesServer
{
    public class PipelinesSocketClientConnection : SocketClientConnection, IDuplexPipe
    {
        public PipeReader Input { get; }
        public PipeWriter Output { get; }
        
        public PipelinesSocketClientConnection(Socket socket) : base(socket)
        {
            var pipe = new Pipe();
            Input = pipe.Reader;
            Output = pipe.Writer;
        }
    }
}