using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UniChat.Server.Core;

namespace UniChat.Server.PipelinesServer
{
    public class PipelinesGameServer : SocketServer<PipelinesSocketClientConnection>
    {   
        public override void OnStarted(EndPoint endPoint)
        {
            throw new NotImplementedException();
        }

        public override void OnClientFaulted(PipelinesSocketClientConnection client, Exception exception)
        {
            throw new NotImplementedException();
        }

        public override Task OnClientConnectedAsync(PipelinesSocketClientConnection client)
        {
            throw new NotImplementedException();
        }

        public override Task OnClientDisconnectedAsync(PipelinesSocketClientConnection client)
        {
            throw new NotImplementedException();
        }

        public override PipelinesSocketClientConnection CreateConnection(Socket socket)
        {
            return new PipelinesSocketClientConnection(socket);
        }
    }
}