﻿using System;
using System.Net;
using System.Threading.Tasks;
using UniChat.Server.AsyncServer;
using UniChat.Server.Core;
using UniChat.Server.PipelinesServer;

namespace UniChat.Server
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {   
            try
            {
                var server = new AsyncGameServer(); // new PipelinesGameServer();
                ListenSocketServer(server);
                AppDomain.CurrentDomain.ProcessExit += (sender, eventArgs) => { StopAsync(server).Wait(); };

                Console.WriteLine("Press any key to stop the server.");
                Console.ReadKey();
                
                await StopAsync(server);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return 1;
            }

            return 0;
        }

        static void ListenSocketServer<T>(IGameServer<T> server)
            where T : SocketClientConnection
        {
            var endPoint = new IPEndPoint(IPAddress.Loopback, 1234);
            server.Listen(endPoint);
        }

        private static async Task StopAsync<T>(IGameServer<T> server)
            where T : IClientConnection
        {
            await server.StopAsync();
            server.Dispose();
        }
    }
}